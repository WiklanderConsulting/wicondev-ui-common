import sbt._
import sbt.Keys._

abstract class WicondevSettings(projectName: String) {
  val organization   = "se.wicondev"
  val name           = s"Wicondev $projectName"
  val normalizedName = s"wicondev-${projectName replace(' ', '-') toLowerCase}"

  val homepage        = Some(url(s"https://bitbucket.org/WiklanderConsulting/$normalizedName"))
  val licenses        = ("MIT License", url("http://www.opensource.org/licenses/mit-license.php"))

  val scmInfo = Some(ScmInfo(
       url(s"https://bitbucket.org/WiklanderConsulting/$normalizedName"),
           s"scm:git:git@bitbucket.org/WiklanderConsulting/$normalizedName.git",
      Some(s"scm:git:git@bitbucket.org/WiklanderConsulting/$normalizedName.git"))
    )

  def publishTo() = (v: String) =>
    if (v.trim.endsWith("SNAPSHOT"))
      Some("snapshots" at mavenBucket + "snapshots")
    else
      Some("releases"  at mavenBucket + "releases")

  val pomExtra =
    <developers>
      <developer>
        <id>PerWiklander</id>
        <name>Per Wiklander</name>
        <url>https://bitbucket.org/perwiklander</url>
      </developer>
    </developers>


  /** Options for the scala compiler */
  val scalacOptions = Seq(
    "-deprecation",            // Emit warning and location for usages of deprecated APIs.
    "-feature",                // Emit warning and location for usages of features that should be imported explicitly.
    "-unchecked",              // Enable additional warnings where generated code depends on assumptions.
    "-Ywarn-inaccessible",     // Warn about inaccessible types in method signatures.
    "-Ywarn-nullary-override"  // Warn when non-nullary overrides nullary, e.g. def foo() over def foo.
    //"-Ywarn-dead-code",        // Warn when dead code is identified.
    //"-Xfatal-warnings",        // Fail the compilation if there are any warnings.
    //"-Xlint",                  // Enable recommended additional warnings.
    //"-Ywarn-adapted-args",     // Warn if an argument list is modified to match the receiver.
    //"-Ywarn-numeric-widen"     // Warn when numerics are widened.
  )

  /** Declare global dependency versions here to avoid mismatches in multi part dependencies */
  object versions {
    val scala        = "2.11.7"
    val scalatags    = "0.5.3"
    val scalajsReact = "0.10.2"
  }

  val mavenBucket = "s3://maven.wicondev.se.s3.amazonaws.com/"

  val snapshotsRepo = s"${mavenBucket}snapshots"
  val releasesRepo  = s"${mavenBucket}releases"

  /** Resolvers needed by project dependencies */
  val resolverSettings: Seq[Def.Setting[_]] = Seq(
    resolvers += "Typesafe Repo" at "http://repo.typesafe.com/typesafe/releases/",
    resolvers += "WiconDev Snapshots" at snapshotsRepo,
    resolvers += "WiconDev Releases"  at releasesRepo
  )
}
