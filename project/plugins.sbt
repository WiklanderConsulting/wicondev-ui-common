logLevel := Level.Warn

addSbtPlugin("com.frugalmechanic"   % "fm-sbt-s3-resolver"  % "0.4.0")
addSbtPlugin("org.scala-js"         % "sbt-scalajs"         % "0.6.7")
