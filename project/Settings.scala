import sbt._
import org.scalajs.sbtplugin.ScalaJSPlugin.autoImport._

/**
 * Application settings. Configure the build for your application here.
 * You normally don't have to touch the actual build definition after this.
 */
object Settings extends WicondevSettings("UI Common") {
  /** The version of your application */
  val version = "0.3.0"

  /**
   * These dependencies are shared between JS and JVM projects
   * the special %%% function selects the correct version for each project
   */
  val sharedDependencies = Def.setting(Seq(
    "com.lihaoyi"   %%% "autowire"    % "0.2.5",
    "com.lihaoyi"   %%% "upickle"     % "0.3.6",
    "com.lihaoyi"   %%% "utest"       % "0.4.3",
    "com.beachape"  %%% "enumeratum"  % "1.3.6"
  ))

  /** Dependencies only used by the JVM project */
  val jvmDependencies = Def.setting(Seq(
    "com.lihaoyi" %%% "scalatags" % versions.scalatags
  ))

  /** Dependencies only used by the JS project (note the use of %%% instead of %%) */
  val scalajsDependencies = Def.setting(Seq(
    "com.github.japgolly.scalajs-react" %%% "core"                    % versions.scalajsReact,
    "com.github.japgolly.scalajs-react" %%% "extra"                   % versions.scalajsReact,
    "com.github.japgolly.scalajs-react" %%% "test"                    % versions.scalajsReact     % "test"
  ))
  
  /** Dependencies for external JS libs that are bundled into a single .js file according to dependency order */
  val jsDependencies = Def.setting(Seq(
    "org.webjars.bower" % "react"           % "0.14.3"  / "react-with-addons.js"  minified "react-with-addons.min.js" commonJSName "React",
    "org.webjars.bower" % "react"           % "0.14.3"  / "react-dom.js"          minified "react-dom.min.js"         commonJSName "ReactDOM" dependsOn "react-with-addons.js",
    "org.webjars"       % "log4javascript"  % "1.4.10"  / "js/log4javascript_uncompressed.js"
  ))
}
