package wicondev.ui.text

import wicondev.ui.text.all._

trait Util {
  def aria(key: String, value: String): Modifier = s"aria-$key".attr := value
  def data(key: String, value: String): Modifier = s"data-$key".attr := value

  /**
   * Returns the given fragment(s) if the given predicate is true or hides it. 
   * 
   * @example         maybe(showTheSpan)(span(cls:="some-class")("My Span"))
   * 
   * @param predicate The boolean that is checked  
   * @param f         The fragment to show if the predicate is true 
   * @return          The given fragment(s) or an empty string
   */
  def maybe(predicate: Boolean)(f: Frag*): Frag = {
    if(predicate) f else Seq.empty[Frag]
  }
}
