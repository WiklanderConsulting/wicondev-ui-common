package wicondev.ui.text

import wicondev.ui.generic.TypedClass

import scalatags.Text.Aggregate
import scalatags.Text.attrs._

trait ClassSet extends Aggregate {
  final def compositeAttr[A](k: Attr, f: (A, List[A]) => A, e: => Modifier) = new CompositeAttr(k, f, e)

  val classSwitch = compositeAttr[String](cls, (h, t) => (h::t).mkString(" ").trim, cls:="")

  @inline final def classSet(ps: (String, Boolean)*): Modifier =
    classSwitch(ps.map(p => if (p._2) Some(p._1) else None): _*)

  @inline final def classSetS(cl: String*): Modifier =
    classSet(cl.map((_, true)): _*)

  @inline final def typedClassSet(cl: TypedClass*): Modifier =
    classSet(cl map { c:TypedClass => (c.asString, true) }: _*)

  @inline final def typedClassSet1(a: String, cl: TypedClass*): Modifier =
    classSet1(a, cl map { c:TypedClass => (c.asString, true) }: _*)

  @inline final def typedOptClassSet(cl: Option[TypedClass]*): Modifier =
    classSet(cl map { c: Option[TypedClass] => (c map (_.asString) getOrElse "", c.isDefined) }: _*)

  @inline final def typedOptClassSet1(a: String, cl: Option[TypedClass]*): Modifier =
    classSet1(a, cl map { c: Option[TypedClass] => (c map (_.asString) getOrElse "", c.isDefined) }: _*)

  @inline final def classSet1(a: String, ps: (String, Boolean)*): Modifier =
    classSet((a, true) +: ps: _*)

  @inline final def classSetM(ps: Map[String, Boolean]): Modifier =
    classSet(ps.toSeq: _*)

  @inline final def classSet1M(a: String, ps: Map[String, Boolean]): Modifier =
    classSet1(a, ps.toSeq: _*)

  final class CompositeAttr[A](k: Attr, f: (A, List[A]) => A, e: => Modifier) {
    def apply(as: Option[A]*)(implicit ev: AttrValue[A]): Modifier =
      as.toList.filter(_.isDefined).map(_.get) match {
        case h :: t => k := f(h, t)
        case Nil => e
      }
  }
}
