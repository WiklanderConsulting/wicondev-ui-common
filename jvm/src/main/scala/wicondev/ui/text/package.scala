package wicondev.ui

package object text {
  trait all  
    extends generic.all
    with    scalatags.Text.Cap
    with    scalatags.Text.Styles
    with    scalatags.DataConverters
    with    scalatags.Text.Aggregate
    with    ClassSet
    with    Util

  trait < extends scalatags.Text.Cap
    with scalatags.text.Tags
    with scalatags.text.Tags2

  trait ^ extends scalatags.Text.Cap
    with scalatags.Text.Attrs

  object all extends all
  object < extends <
  object ^ extends ^
}
