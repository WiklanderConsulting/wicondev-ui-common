package wicondev.data.units

import utest._
import Percentage._

object PercentageTest extends TestSuite {
  override val tests = this {
    '+ {
      val p1: Percentage = 100.pct
      val p2: Percentage = 25.pct

      (p1 + p2) ==> 125.pct
      (p2 + p1) ==> 125.pct
    }

    '- {
      val p1: Percentage = 100.pct
      val p2: Percentage = 25.pct

      (p1 - p2) ==> 75.pct
      (p2 - p1) ==> -75.pct
    }

    '* {
      val p1: Percentage = 100.pct
      val p2: Percentage = 25.pct
      val p3: Percentage = 60.pct
      val p4: Percentage = 15.pct

      (p1 * p2) ==> p2
      (p2 * p1) ==> p2

      (p2 * p3) ==> p4
      (p3 * p2) ==> p4
    }

    '/ {
      val p1: Percentage = 100.pct
      val p2: Percentage = 25.pct

      val p3: Percentage = 20.pct
      val p4: Percentage = 50.pct

      (p2 / p1) ==> p2
      (p3 / p4) ==> 40.pct
    }

    '< {
      val p1: Percentage = 1.pct
      val p2: Percentage = 2.pct
      val p3: Percentage = 2.pct

      assert(p1 < p2)
      assert(!(p2 < p1))
      assert(p2 !< p1)

      assert(!(p2 < p3))
      assert(p2 !< p3)
    }

    '> {
      val p1: Percentage = 1.pct
      val p2: Percentage = 2.pct
      val p3: Percentage = 2.pct

      assert(p2 > p1)
      assert(!(p1 > p2))
      assert(p1 !> p2)

      assert(!(p2 > p3))
      assert(p2 !> p3)
    }
  }
}
