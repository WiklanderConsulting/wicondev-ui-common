package wicondev.ui.generic

abstract class TypedClass(value: String*) {
  def asString:          String = value.mkString(" ").trim
  override def toString: String = asString

  def is[T <: TypedClass](classType: T): Boolean =
   classType.asString == asString

  def is[T <: TypedClass](maybeClassType: Option[T]): Boolean =
   maybeClassType.exists(is(_))

  def isOneOf[T <: TypedClass](classType: T*): Boolean =
   classType.exists(is(_))

  def isNot[T <: TypedClass](classType: T): Boolean               = !is(classType)
  def isNot[T <: TypedClass](maybeClassType: Option[T]): Boolean  = !is(maybeClassType)
  def isNoneOf[T <: TypedClass](classType: T*): Boolean           = !isOneOf(classType: _*)
}

object TypedClass {
  import scala.language.implicitConversions
  implicit def typedClass2String(typedClass: TypedClass): String = typedClass.asString
}