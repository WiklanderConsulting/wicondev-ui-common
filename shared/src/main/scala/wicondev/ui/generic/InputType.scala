package wicondev.ui.generic

trait InputType {
  case class InputType(value: String) extends TypedClass(value)

  object InputType {
    val CHECK_BOX = InputType("checkbox")
    val DATE      = InputType("date")
    val EMAIL     = InputType("email")
    val FILE      = InputType("file")
    val NUMBER    = InputType("number")
    val PASSWORD  = InputType("password")
    val RADIO     = InputType("radio")
    val TEL       = InputType("tel")
    val TEXT      = InputType("text")
    val TEXT_AREA = InputType("textarea")
    val SELECT    = InputType("select")
    val STATIC    = InputType("static")
    val SUBMIT    = InputType("submit")
  }
}
