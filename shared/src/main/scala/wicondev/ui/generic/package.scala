package wicondev.ui

package object generic {
  trait all
    extends InputStatus
    with    InputType

  object all extends all
}
