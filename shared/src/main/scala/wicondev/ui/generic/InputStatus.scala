package wicondev.ui.generic

trait InputStatus {
   case class InputStatus(value: String) extends TypedClass(value)

   object InputStatus {
     val SUCCESS = InputStatus("success")
     val WARNING = InputStatus("warning")
     val ERROR   = InputStatus("error")
   }
 }
