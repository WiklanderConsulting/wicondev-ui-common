package wicondev.data.units

import wicondev.data.ValueObject

import scala.language.implicitConversions

case class Percentage(value: Int) extends ValueObject[Int] {
  override def formattedString = s"$asString%"

  def zero: Percentage = Percentage(0)
  def oneHundred: Percentage = Percentage(100)

  def +(p2: Percentage):  Percentage = Percentage(value + p2.value)
  def -(p2: Percentage):  Percentage = Percentage(value - p2.value)
  def *(p2: Percentage):  Percentage = Percentage(Math.floor((value.toDouble / 100) * p2.value).toInt)
  def /(p2: Percentage):  Percentage = Percentage(Math.floor((value.toDouble / 100) / (p2.value.toDouble / 100) * 100).toInt)
  def >(p2: Percentage):  Boolean    = value > p2.value
  def !>(p2: Percentage): Boolean    = !(value > p2.value)
  def <(p2: Percentage):  Boolean    = value < p2.value
  def !<(p2: Percentage): Boolean    = !(value < p2.value)
}

object Percentage {
  implicit class PercentageInteger(value: Int) {
    def pct: Percentage = Percentage(value)
  }
}
