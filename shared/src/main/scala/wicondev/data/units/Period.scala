package wicondev.data.units

import wicondev.data.{Enum, EnumEntry}

sealed trait Period extends EnumEntry
object Period extends Enum[Period] {
  lazy val values: Seq[Period] = findValues
  case object Weekly      extends Period
  case object Monthly     extends Period
  case object Quarterly   extends Period
  case object FourMonths  extends Period
  case object Semiannual  extends Period
  case object Annual      extends Period
}


