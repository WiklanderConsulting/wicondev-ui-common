package wicondev.data.units

import wicondev.data.{Enum, EnumEntry}

sealed trait MeasurementUnit extends EnumEntry

object MeasurementUnit extends Enum[MeasurementUnit] {
  lazy val values: Seq[MeasurementUnit] = findValues
  case object BritishPound  extends MeasurementUnit
  case object Index         extends MeasurementUnit
  case object Euro          extends MeasurementUnit
  case object Percent       extends MeasurementUnit
  case object SvenskaKronor extends MeasurementUnit
  case object UsDollar      extends MeasurementUnit
}
