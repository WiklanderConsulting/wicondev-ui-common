package wicondev.data.units

import wicondev.data.ValueObject

import scala.language.implicitConversions

case class Duration(value: Long) extends ValueObject[Long] {
  def +(other: Duration): Duration    = Duration(value + other.value)
  def +(time:  Timestamp): Timestamp  = time + this
  def -(other: Duration): Duration    = Duration(value - other.value)
}

object Duration {
  implicit def ordering: Ordering[Duration] = Ordering.by(_.value)
}

