package wicondev.data.units

import wicondev.data.ValueObject

import scala.language.implicitConversions

case class Timestamp(value: Long) extends ValueObject[Long] {
  def +(added: Duration): Timestamp     = Timestamp(value + added.value)
  def -(sutracted: Duration): Timestamp = Timestamp(value - sutracted.value)
  def -(sutracted: Timestamp): Duration = Duration(Math.max(sutracted, this) - Math.min(sutracted, this))
}

object Timestamp {
  implicit def ordering: Ordering[Timestamp] = Ordering.by(_.value)
}
