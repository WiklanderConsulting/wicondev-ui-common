package wicondev.data.units

import wicondev.data.{Enum, EnumEntry, ValueObject}

case class Money(amount: Int, currency: Currency) extends ValueObject[String] {
  override def value: String   = s"$amount:$currency"
  override def formattedString = s"$amount $currency"
}

object Money {
  def apply(encodedCost: String): Money = {
    encodedCost.split(':') match {
      case Array(v, c) => Money(v.toInt, Currency.withName(c))
      case _           => throw new IllegalArgumentException(s""""Encoded cost value "$encodedCost" did not match the pattern "value:currency"""")
    }
  }
}

sealed trait Currency extends EnumEntry

object Currency extends Enum[Currency] {
  lazy val values: Seq[Currency] = findValues
  case object GBP extends Currency
  case object EUR extends Currency
  case object SEK extends Currency
  case object USD extends Currency
}
