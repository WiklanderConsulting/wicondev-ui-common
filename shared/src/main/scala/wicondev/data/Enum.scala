package wicondev.data

abstract class EnumEntry extends enumeratum.EnumEntry with ValueObject[String]  {
  override def entryName = getClass.getSimpleName.replace("$", "")
  override def value     = entryName
}

abstract class Enum[A <: EnumEntry] extends enumeratum.Enum[A]
