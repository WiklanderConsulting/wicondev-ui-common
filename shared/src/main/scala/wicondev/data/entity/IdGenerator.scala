package wicondev.data.entity

import scala.util.Random

/**
 * 
 * @param prefix Concrete id generators supply this to define the prefix needed to generate new ids.
 * @tparam Id The type of the id this generator produces
 */
abstract class IdGenerator[Id <: Identifier](prefix: String) {
  /**
   * Generate a new random id of type `Type` prefixed according to the settings in the generator function.
 *
   * @return
   */
  def random: Id = generateId(prefix)

  def apply(id: String): Id
  
  /**
   * Constructs a new id of type `Type` with the given prefix
   */
  private def generateId(prefix: String = "", numChars: Int = 20): Id = 
     apply(prefixedString(prefix, numChars))
  
  /**
    * Generates an id string
    * 
    * @param prefix    Optional prefix to make the generated opaque id more understandable.
    * @param numChars  Number of chars to generate. Defaults to 20 which is about one order of magnitude less possible
    *                  values than a UUID.
    * @return          A string representing the generated (probably unique) id
    */
   private def prefixedString(prefix: String, numChars: Int = 20): String = {
     if(prefix.isEmpty) {
       randomString(numChars)
     } else {
       s"${prefix}_${randomString(numChars)}"
     }
   }
  
   private def randomString(numChars: Int): String = new String((Random.alphanumeric take numChars).toArray)
 }
