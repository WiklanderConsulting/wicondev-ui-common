package wicondev.data.entity

import wicondev.data.ValueObject

trait Identifier extends ValueObject[String]
