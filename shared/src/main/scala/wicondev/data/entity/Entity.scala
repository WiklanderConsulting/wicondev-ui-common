package wicondev.data.entity

trait Entity[Id] {
  def id: Id
}
