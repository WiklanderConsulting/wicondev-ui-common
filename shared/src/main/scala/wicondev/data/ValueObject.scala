package wicondev.data

trait ValueObject[Type] {
  def value: Type
  def asString          = value.toString
  def formattedString   = asString
  override def toString = asString
}

object ValueObject {
  import scala.language.implicitConversions

  implicit def optionalValueObject2String[Value <: ValueObject[_]](maybeValueObject: Option[Value]): Option[String] = maybeValueObject map(_.asString)
  implicit def valueObject2String[Type](valueObject: ValueObject[Type]):    String  = valueObject.asString
  implicit def valueObject2Boolean(     valueObject: ValueObject[Boolean]): Boolean = valueObject.value
  implicit def valueObject2Long(        valueObject: ValueObject[Long]):    Long    = valueObject.value
  implicit def valueObject2Int(         valueObject: ValueObject[Int]):     Int     = valueObject.value
  implicit def valueObject2Double(      valueObject: ValueObject[Double]):  Double  = valueObject.value

  implicit def orderingByStringValue [T <: ValueObject[String]]:  Ordering[T] = Ordering.by(_.value)
  implicit def orderingByBooleanValue[T <: ValueObject[Boolean]]: Ordering[T] = Ordering.by(_.value)
  implicit def orderingByLongValue   [T <: ValueObject[Long]]:    Ordering[T] = Ordering.by(_.value)
  implicit def orderingByIntValue    [T <: ValueObject[Int]]:     Ordering[T] = Ordering.by(_.value)
  implicit def orderingByDoubleValue [T <: ValueObject[Double]]:  Ordering[T] = Ordering.by(_.value)
}
