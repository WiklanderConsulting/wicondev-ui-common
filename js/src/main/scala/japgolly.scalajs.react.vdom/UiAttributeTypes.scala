package japgolly.scalajs.react.vdom

import japgolly.scalajs.react.vdom.Scalatags.GenericAttr
import wicondev.ui.generic.TypedClass

trait UiAttributeTypes {
  @inline implicit final def typedClass2Attr[T <: TypedClass]: AttrValue[T] =
    new GenericAttr[T](_.asString)
}
