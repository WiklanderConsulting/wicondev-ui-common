package wicondev.data.utils

import wicondev.data.units.Timestamp

import scala.scalajs.js.Date
import scala.util.{Success, Failure, Try}

object TimeUtils {
  def formatIsoTime(timestamp: Timestamp): String = {
    val date = new Date(timestamp.value)
    "%02d:%02d:%02d".format(date.getHours(), date.getMinutes(), date.getSeconds())
  }
  
  def formatIsoDate(timestamp: Timestamp): String = {
    val date = new Date(timestamp.value)
    "%04d-%02d-%02d".format(date.getFullYear(), date.getMonth() + 1, date.getDate())
  }

  def formatIsoDateTime(timestamp: Timestamp): String = {
    s"${formatIsoDate(timestamp)} ${formatIsoTime(timestamp)}"
  }
  
  def isoDateToTimestamp(dateString: String): Try[Timestamp] = {
    val millis = Date.parse(dateString)
      if(millis.isNaN) {
        Failure(new IllegalArgumentException(s"""The given date string "$dateString" could not be parsed to a valid timestamp"""))
      } else {
        Success(Timestamp(millis.toLong))
      }
    }
  
  object implicits {
    import scala.language.implicitConversions
    
    implicit def date2Timestamp(date: Date): Timestamp = Timestamp(date.getTime().toLong)
    implicit def timestamp2Date(timestamp: Timestamp): Date = new Date(timestamp.value)
    
    def now = new scala.scalajs.js.Date()
  }
}
