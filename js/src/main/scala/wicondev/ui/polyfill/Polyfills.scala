package wicondev.ui.polyfill

trait Polyfills {
  def applyAllPolyfills(): Unit = {
    locationOrigin()
  }

  /**
   * window.location.origin is missing in < IE11
   *
   * @see http://tosbourn.com/a-fix-for-window-location-origin-in-internet-explorer/
   */
  def locationOrigin(): Unit = {
    import scala.scalajs.js.Dynamic.{global => g}
    import scala.scalajs.js.DynamicImplicits._
    if (!g.window.location.origin) {
      g.window.location.origin = g.window.location.protocol +
        "//" +
        g.window.location.hostname +
        (if (g.window.location.port) ":" + g.window.location.port else "")
    }
  }
}
