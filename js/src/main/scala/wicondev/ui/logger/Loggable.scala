package wicondev.ui.logger

/**
 * Extend this to get a named logger in scope.
 */
trait Loggable {
  /**
   * A named logger instance.
   */
  val logger: Logger = LoggerFactory.getLogger(this.getClass.getSimpleName)
}
