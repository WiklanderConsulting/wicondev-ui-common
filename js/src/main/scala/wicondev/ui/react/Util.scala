package wicondev.ui.react

import japgolly.scalajs.react.ReactElement
import japgolly.scalajs.react.vdom.Attr
import wicondev.ui.react.all._

trait Util {
  def data(key: String, value: String): TagMod = Attr(s"data-$key") := value

  /**
   * Returns the given element(s) if the given predicate is true or an empty element list. 
   * 
   * @example         maybe(showTheSpan)(span(className:="some-class")("My Span"))
   *
   * @param predicate The boolean that is checked
   * @param elements  The elements to maybe show
   * @return          The given element(s) or an empty element list
   */
  def maybe(predicate: Boolean)(elements: ReactElement*): Seq[ReactElement] =
    maybeS(predicate)(elements)
  
  /**
   * Returns the given element(s) if the given predicate is true or an empty element list. 
   * 
   * @example         maybe(showTheSpan)(span(className:="some-class")("My Span"))
   * @param predicate The boolean that is checked
   * @param elements  The elements to maybe show
   * @return          The given element(s) or an empty element list
   */
  def maybeS(predicate: Boolean)(elements: Seq[ReactElement]): Seq[ReactElement] =
    if(predicate) elements
    else Seq.empty[ReactElement]
  
  /**
   * Returns the given element(s) if the given predicate is false or an empty element list. 
   * 
   * @example         maybe(showTheSpan)(span(className:="some-class")("My Span"))
   * @param predicate The boolean that is checked
   * @param elements  The elements to maybe show
   * @return          The given element(s) or an empty element list
   */
  def maybeNot(predicate: Boolean)(elements: ReactElement*): Seq[ReactElement] =
    maybeNotS(predicate)(elements)
  
  /**
   * Returns the given element(s) if the given predicate is false or an empty element list. 
   * 
   * @example         maybe(showTheSpan)(span(className:="some-class")("My Span"))
   * @param predicate The boolean that is checked
   * @param elements  The elements to maybe show
   * @return          The given element(s) or an empty element list
   */
  def maybeNotS(predicate: Boolean)(elements: Seq[ReactElement]): Seq[ReactElement] =
    if(!predicate) elements
    else Seq.empty[ReactElement]
  
  /**
   * Returns a list of produced elements or an empty element list if None is given.
   * 
   * @example maybe(couldBeSomething)(something => span(className:="some-class")(something.someOther))
   * @param maybeSomething Something which might turn into an elelment list
   * @param f              A function that turns something into a list of elements
   * @return The produced element(s) or an empty element list
   */
  def maybe[Something](maybeSomething: Option[Something])(f: Something => ReactElement): Seq[ReactElement] =
    maybeSomething map (s => Seq(f(s))) getOrElse Seq.empty[ReactElement]

  /**
   * Returns a list of elements if the given Option is empty or an empty list. 
   * 
   * @example maybeNot(shouldBeNothing)(span(className:="some-class")("My Span"))
   * @param shouldBeNothing Something which should not exist
   * @param elements       A list of elements to maybe show
   * @return The given element(s) or an empty element list
   */
  def maybeNot[Something](shouldBeNothing: Option[Something])(elements: ReactElement*) =
    maybeS(shouldBeNothing.isEmpty)(elements)
}
