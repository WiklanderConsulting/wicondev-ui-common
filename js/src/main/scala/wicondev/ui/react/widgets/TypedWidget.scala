package wicondev.ui.react.widgets

import japgolly.scalajs.react.{ReactComponentB, ReactElement, TopNode}

trait TypedWidget[Props, DomType <: TopNode] extends WidgetBase[Props, Unit, Unit, DomType] {
  protected override val widget: Widget =
    ReactComponentB[Props](name)
      .render                     (render)
      .domType[DomType]
      .configure                  (configure: _*)
      .componentWillMount         (componentWillMount)
      .componentDidMount          (componentDidMount)
      .componentWillReceiveProps  (componentWillReceiveProps)
      .componentWillUpdate        (componentWillUpdate)
      .componentDidUpdate         (componentDidUpdate)
      .componentWillUnmount       (componentWillUnMount)
      .build

  protected def render: Scope => ReactElement
}
