package wicondev.ui.react.widgets

import japgolly.scalajs.react._
import japgolly.scalajs.react.ReactComponentC.ReqProps

import scala.scalajs.js

protected trait WidgetBase[Props, State, Backend, DomType <: TopNode] {
  protected type ComponentB = ReactComponentB[Props, State, Backend, DomType]
  protected type ComponentU = ReactComponentU[Props, State, Backend, DomType]
  protected type Scope      = CompScope.DuringCallbackU[Props, State, Backend]
  protected type ScopeM     = CompScope.DuringCallbackM[Props, State, Backend, DomType]
  protected type Widget     = ReqProps[Props, State, Backend, DomType]

  protected def widget: Widget

  def apply(props: Props, children: ReactNode*): ComponentU =  widget(props, children)

  def withKey(key: js.Any): Widget = widget.set(key = key)
  def withRef(ref: String): Widget = widget.set(ref = ref)

  protected def name: String = this.getClass.getSimpleName

  protected def configure: Seq[ComponentB => ComponentB] = Seq.empty[ComponentB => ComponentB]

  protected def componentWillMount:        Scope                                                     => Callback = $ => Callback.empty
  protected def componentDidMount:         ScopeM                                                    => Callback = $ => Callback.empty
  protected def componentWillUnMount:      ScopeM                                                    => Callback = $ => Callback.empty
  protected def componentWillReceiveProps: ComponentWillReceiveProps[Props, State, Backend, DomType] => Callback = $ => Callback.empty
  protected def componentWillUpdate:       ComponentWillUpdate      [Props, State, Backend, DomType] => Callback = $ => Callback.empty
  protected def componentDidUpdate:        ComponentDidUpdate       [Props, State, Backend, DomType] => Callback = $ => Callback.empty
}
