package wicondev.ui.react.widgets

import japgolly.scalajs.react.Callback
import wicondev.ui.react.all._

trait ItemSelectorProps[ItemType] {
  def id:       Option[String]        = None
  def items:    Seq[ItemType]         = Seq()
  def selected: ItemType
  def onChange: ItemType => Callback  = _ => Callback.empty
  def label:    Option[String]        = None
  def hasError: Boolean               = false
  def asValue:  ItemType => String    = (item: ItemType) => item.toString
  def asLegend: ItemType => TagMod    = (item: ItemType) => item.toString
  val itemMap:  Map[String, ItemType] = Map(items map (item => asValue(item) -> item): _*)
}

trait ItemSelector[ItemType] extends Widget[ItemSelectorProps[ItemType]]
