package wicondev.ui.react.widgets

import japgolly.scalajs.react._
import japgolly.scalajs.react.{ReactComponentB, ReactElement, ReactEventI, TopNode}

trait TypedStatefulWidget[Props, State, DomType <: TopNode] extends WidgetBase[Props, State, Unit, DomType] {
  protected override val widget: Widget =
    ReactComponentB[Props](name)
      .initialState_P(initialState)
      .render(render)
      .domType[DomType]
      .configure(configure: _*)
      .componentWillMount(componentWillMount)
      .componentDidMount(componentDidMount)
      .componentWillReceiveProps(componentWillReceiveProps)
      .componentWillUpdate(componentWillUpdate)
      .componentDidUpdate(componentDidUpdate)
      .componentWillUnmount(componentWillUnMount)
      .build

  protected def initialState: Props => State

  protected def render: Scope => ReactElement

  protected def modifyStateFromEventTarget(
    modFromString: String => State => State,
    $: Scope
  ): ReactEventI => Callback =
    (e: ReactEventI) => $.modState(modFromString(e.target.value))
}
