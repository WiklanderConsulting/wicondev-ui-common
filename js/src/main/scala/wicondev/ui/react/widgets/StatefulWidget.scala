package wicondev.ui.react.widgets

import japgolly.scalajs.react.TopNode

trait StatefulWidget[Props, State] extends TypedStatefulWidget[Props, State, TopNode]
