package wicondev.ui.react.widgets

import japgolly.scalajs.react.Callback
import wicondev.ui.react.all._

trait MultipleItemSelectorProps[ItemType] {
  def id:       Option[String]
  def items:    Seq[ItemType]
  def selected: Seq[ItemType]
  def onChange: Seq[ItemType] => Callback
  def label:    Option[String]
  def hasError: Boolean
  def asValue:  ItemType => String    = (item: ItemType) => item.toString
  def asLegend: ItemType => TagMod    = (item: ItemType) => item.toString
  val itemMap:  Map[String, ItemType] = Map(items map (item => asValue(item) -> item): _*)
}

case class MultipleItemSelectorState[ItemType](
  selected: Seq[ItemType]
)

trait MultipleItemSelector[ItemType] extends StatefulWidget[
  MultipleItemSelectorProps[ItemType],
  MultipleItemSelectorState[ItemType]
]
{
  override def initialState = P => MultipleItemSelectorState(P.selected)
}
