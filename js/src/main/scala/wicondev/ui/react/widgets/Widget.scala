package wicondev.ui.react.widgets

import japgolly.scalajs.react.TopNode

trait Widget[Props] extends TypedWidget[Props, TopNode]
