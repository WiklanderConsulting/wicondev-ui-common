package wicondev.ui.react.widgets

import japgolly.scalajs.react.CallbackOption._
import japgolly.scalajs.react._
import org.scalajs.dom.ext.KeyCode
import wicondev.data.units.{Percentage, Timestamp}
import wicondev.data.utils.TimeUtils
import wicondev.ui.react.all._

import scala.util.{Success, Try}

trait TypedInput {
  trait TypedInputTypes[Type] {
    type MaybeType      = Option[Type]
    type MaybeCallback  = Option[Callback]
    type TypeToCallback = MaybeType => MaybeCallback
  }

  trait TypedInputProps[Type] extends TypedInputTypes[Type] {
    def id:          Option[String]
    def ref:         Option[Ref]
    def autoFocus:   Option[Boolean]
    def placeholder: Option[String]
    def value:       MaybeType
    def disabled:    Boolean
    def readOnly:    Boolean
    def onChange:    TypeToCallback
    def onBlur:      TypeToCallback
    def onSubmit:    TypeToCallback
    def onEscape:    TypeToCallback
  }

  trait TypedInputBase[Type] extends Widget[TypedInputProps[Type]] with TypedInputTypes[Type] {
    protected def htmlType: InputType = InputType.TEXT

    protected def valueFromType(t: Type): String = t.toString

    protected def valueToType(v: String): Try[Type]

    protected def delegateEvent(
      event: ReactEventI,
      handler: TypeToCallback
    ): MaybeCallback = {
      val value = event.target.value
      if (value.isEmpty) handler(None) else handler(valueToType(value).toOption)
    }

    protected def onChange(implicit $: Scope): ReactEventI => MaybeCallback =
      event => delegateEvent(event, $.props.onChange)

    protected def onBlur(implicit $: Scope): ReactEventI => MaybeCallback =
      event => delegateEvent(event, $.props.onBlur)

    protected def onKeyDown(implicit $: Scope): ReactKeyboardEventI => Callback =
      event => keyCodeSwitch(event) {
        case KeyCode.Enter  =>
          delegateEvent(event, $.props.onSubmit) map(_ >> preventDefault(event)) getOrElse Callback.empty
        case KeyCode.Escape =>
          delegateEvent(event, $.props.onEscape) map(_ >> preventDefault(event)) getOrElse Callback.empty
      } orElse invalidKeys(event)

    protected def invalidKeys: ReactKeyboardEventI => CallbackOption[Unit] =
      event => unless(validKey(event.keyCode)) >> preventDefault(event)

    protected def validKey(keyCode: Int): Boolean = true

    protected def inputElement(implicit $: Scope): ReactTag =
      <.input(
        ^.tpe            := htmlType,
        ^.id             := $.props.id,
        ^.ref            := $.props.ref,
        ^.autoFocus      := $.props.autoFocus,
        ^.value          := $.props.value map valueFromType,
        $.props.disabled ?= (^.disabled := "disabled"),
        $.props.readOnly ?= (^.readOnly := "readOnly"),
        ^.placeholder    := $.props.placeholder,
        ^.onChange       ==>? onChange,
        ^.onBlur         ==>? onBlur,
        ^.onKeyDown      ==> onKeyDown
      )

    override protected def render = $ => {
      implicit val scope: Scope = $
      inputElement
    }
  }

  trait StringInput extends TypedInputBase[String] {
    case class Props(
      id:          Option[String]                   = None,
      ref:         Option[Ref]                      = None,
      autoFocus:   Option[Boolean]                  = None,
      placeholder: Option[String]                   = None,
      value:       MaybeType                        = None,
      disabled:    Boolean                          = false,
      readOnly:    Boolean                          = false,
      onChange:    TypeToCallback = (ot: MaybeType) => None,
      onBlur:      TypeToCallback = (ot: MaybeType) => None,
      onSubmit:    TypeToCallback = (ot: MaybeType) => None,
      onEscape:    TypeToCallback = (ot: MaybeType) => None
    ) extends TypedInputProps[String]

    override protected def valueToType(v: String): Try[String] = Success(v)
  }

  object TextInput extends StringInput

  object MultiLineTextInput extends StringInput {
    // Special treatment of enter key in textarea since we don't want normal line breaks to submit
    override protected def onKeyDown(implicit $: Scope): ReactKeyboardEventI => Callback =
      event => {
        def onEnter: PartialFunction[Int, CallbackTo[Unit]] = {
          case KeyCode.Enter  =>
            delegateEvent(event, $.props.onSubmit) map(_ >> preventDefault(event)) getOrElse Callback.empty
        }

        def onEsc: PartialFunction[Int, CallbackTo[Unit]] = {
          case KeyCode.Escape =>
            delegateEvent(event, $.props.onEscape) map (_ >> preventDefault(event)) getOrElse Callback.empty
        }

        keyCodeSwitch(event)(onEsc)
        .orElse(keyCodeSwitch(event, metaKey = true)(onEnter))
        .orElse(keyCodeSwitch(event, ctrlKey = true)(onEnter))
        .orElse(invalidKeys(event))
      }

    override protected def render = $ => {
      implicit val scope: Scope = $

      <.textarea(
        ^.id             := $.props.id,
        ^.ref            := $.props.ref,
        ^.autoFocus      := $.props.autoFocus,
        ^.value          := $.props.value map valueFromType,
        $.props.disabled ?= (^.disabled := "disabled"),
        $.props.readOnly ?= (^.readOnly := "readOnly"),
        ^.placeholder    := $.props.placeholder,
        ^.onChange       ==>? onChange,
        ^.onBlur         ==>? onBlur,
        ^.onKeyDown      ==> onKeyDown
      )
    }
  }

  object EmailInput extends StringInput {
    override protected val htmlType = InputType.EMAIL
  }

  object PhoneInput extends StringInput {
    override protected val htmlType = InputType.TEL
  }

  trait NumericInput[Number] extends TypedInputBase[Number] {
    //TODO: Implement proper numeric functionality. See: https://github.com/EmKayDK/jstepper
    override protected val htmlType = InputType.NUMBER
  }

  object IntInput extends NumericInput[Int] {
    override protected def valueToType(v: String): Try[Int] = Try(v.toInt)

    case class Props(
      id:          Option[String]                   = None,
      ref:         Option[Ref]                      = None,
      autoFocus:   Option[Boolean]                  = None,
      placeholder: Option[String]                   = None,
      value:       MaybeType                        = None,
      disabled:    Boolean                          = false,
      readOnly:    Boolean                          = false,
      onChange:    TypeToCallback = (ot: MaybeType) => None,
      onBlur:      TypeToCallback = (ot: MaybeType) => None,
      onSubmit:    TypeToCallback = (ot: MaybeType) => None,
      onEscape:    TypeToCallback = (ot: MaybeType) => None
    ) extends TypedInputProps[Int]
  }

  object LongInput extends NumericInput[Long] {
    override protected def valueToType(v: String): Try[Long] = Try(v.toLong)

    case class Props(
      id:          Option[String]                   = None,
      ref:         Option[Ref]                      = None,
      autoFocus:   Option[Boolean]                  = None,
      placeholder: Option[String]                   = None,
      value:       MaybeType                        = None,
      disabled:    Boolean                          = false,
      readOnly:    Boolean                          = false,
      onChange:    TypeToCallback = (ot: MaybeType) => None,
      onBlur:      TypeToCallback = (ot: MaybeType) => None,
      onSubmit:    TypeToCallback = (ot: MaybeType) => None,
      onEscape:    TypeToCallback = (ot: MaybeType) => None
    ) extends TypedInputProps[Long]
  }

  object PercentageInput extends NumericInput[Percentage] {
    override protected def inputElement(implicit $: Scope): ReactTag = super.inputElement($)(^.min := 0, ^.max := 100)

    override protected def valueToType(v: String): Try[Percentage] = Try(Percentage(v.toInt))

    case class Props(
      id:          Option[String]                   = None,
      ref:         Option[Ref]                      = None,
      autoFocus:   Option[Boolean]                  = None,
      placeholder: Option[String]                   = None,
      value:       MaybeType                        = None,
      disabled:    Boolean                          = false,
      readOnly:    Boolean                          = false,
      onChange:    TypeToCallback = (ot: MaybeType) => None,
      onBlur:      TypeToCallback = (ot: MaybeType) => None,
      onSubmit:    TypeToCallback = (ot: MaybeType) => None,
      onEscape:    TypeToCallback = (ot: MaybeType) => None
    ) extends TypedInputProps[Percentage]
  }

  object DateInput extends TypedInputBase[Timestamp] {
    case class Props(
      id:          Option[String]                   = None,
      ref:         Option[Ref]                      = None,
      autoFocus:   Option[Boolean]                  = None,
      placeholder: Option[String]                   = None,
      value:       MaybeType                        = None,
      disabled:    Boolean                          = false,
      readOnly:    Boolean                          = false,
      onChange:    TypeToCallback = (ot: MaybeType) => None,
      onBlur:      TypeToCallback = (ot: MaybeType) => None,
      onSubmit:    TypeToCallback = (ot: MaybeType) => None,
      onEscape:    TypeToCallback = (ot: MaybeType) => None
    ) extends TypedInputProps[Timestamp]

    override protected def htmlType: InputType = InputType.DATE
    override protected def valueToType(v: String): Try[Timestamp] = TimeUtils.isoDateToTimestamp(v)
    override protected def valueFromType(t: Timestamp): String    = TimeUtils.formatIsoDate(t)
  }
}
