package wicondev.ui.react.widgets

import japgolly.scalajs.react._

/**
 * Create a widget that always displays the same content, never needs to be redrawn, never needs vdom diffing.
 */
trait StaticWidget {
  private[StaticWidget] val widget = ReactComponentB.static(name, render).buildU

  def apply(children: ReactElement*) = widget(children)

  protected def name: String = this.getClass.getSimpleName
  protected def render: ReactElement
}
