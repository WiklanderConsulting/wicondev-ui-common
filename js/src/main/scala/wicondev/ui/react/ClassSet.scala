package wicondev.ui.react

import japgolly.scalajs.react.vdom.all._
import wicondev.ui.generic.TypedClass

trait ClassSet {
  @inline final def classSetS(cs: String*): Seq[TagMod] =
    cs map(c => cls:=c)
  
  @inline final def classSet(ps: (String, Boolean)*): Seq[TagMod] =
    classSetS(ps map (p => if(p._2) p._1 else "" ): _*)

  @inline final def classSet1(a: String, ps: (String, Boolean)*): Seq[TagMod] =
    classSet((a, true) +: ps: _*)

  @inline final def classSetO(cl: Option[String]*): Seq[TagMod] =
    classSetS(cl.map(_ getOrElse ""): _*)

  @inline final def typedClassSet(cl: TypedClass*): Seq[TagMod] =
    classSetS(cl map (_.asString): _*)

  @inline final def typedClassSet1(a: String, tps: TypedClass*): Seq[TagMod] =
    Seq(cls:=a, typedClassSet(tps: _*))

  @inline final def typedOptClassSet(cl: Option[TypedClass]*): Seq[TagMod] =
    classSetS(cl map (_ map (_.asString) getOrElse ""): _*)

  @inline final def typedOptClassSet1(a: String, cl: Option[TypedClass]*): Seq[TagMod] =
    Seq(cls:=a, typedOptClassSet(cl: _*))

  @inline final def classSetM(ps: Map[String, Boolean]): Seq[TagMod] =
    classSet(ps.toSeq: _*)

  @inline final def classSet1M(a: String, ps: Map[String, Boolean]): Seq[TagMod] =
    classSet1(a, ps.toSeq: _*)
}

