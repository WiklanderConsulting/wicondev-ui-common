package wicondev.ui.react

import wicondev.ui.react.all._

trait Field {
  /**
   * A form field.
   *
   * @param label the field label
   * @param constraints the constraints associated with the field
   * @param format the format expected for this field
   * @param errors the errors associated to this field
   * @param defaultValue the field value, if any
   */
  case class Field(
    label: String,
    inputType: InputType = InputType.TEXT,
    constraints: Seq[(String, Seq[Any])] = Seq(),
    format: Option[(String, Seq[Any])] = None,
    errors: Seq[FormError] = Seq(),
    defaultValue: Option[String] = None,
    onChange: Option[String => Any] = Some((value: String) => Unit)
    ) {

    /**
     * The field ID - the same as the field name but with '.' replaced by '_'.
     */
    lazy val id: String = name.replace('.', '_').replace('[', '_').replace("]", "")

    /**
     * The field name - the same as the field label but with ' ' replaced by '_'.
     */
    lazy val name: String = label.replace(" ", "_").toLowerCase

    /**
     * Returns the first error associated with this field, if it exists.
     *
     * @return an error
     */
    lazy val error: Option[FormError] = errors.headOption

    /**
     * Check if this field has errors.
     */
    lazy val hasErrors: Boolean = errors.nonEmpty
  }

  /**
   * A form error.
   *
   * @param key The error key (should be associated with a field using the same key).
   * @param messages The form message (often a simple message key needing to be translated).
   * @param args Arguments used to format the message.
   */
  case class FormError(key: String, messages: Seq[String], args: Seq[Any] = Nil) {

    def this(key: String, message: String) = this(key, Seq(message), Nil)

    def this(key: String, message: String, args: Seq[Any]) = this(key, Seq(message), args)

    lazy val message = messages.last

    /**
     * Copy this error with a new Message.
     *
     * @param message The new message.
     */
    def withMessage(message: String): FormError = FormError(key, message)
  }

  object FormError {
    def apply(key: String, message: String) = new FormError(key, message)

    def apply(key: String, message: String, args: Seq[Any]) = new FormError(key, message, args)
  }
}
