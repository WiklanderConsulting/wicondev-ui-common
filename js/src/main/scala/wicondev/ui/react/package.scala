package wicondev.ui

package object react {
  trait all
    extends japgolly.scalajs.react.vdom.Base
    with    japgolly.scalajs.react.vdom.UiAttributeTypes
    with    generic.all
    with    polyfill.Polyfills
    with    ClassSet
    with    Field
    with    widgets.TypedInput
    with    Util
  {
    import japgolly.scalajs.react.vdom.prefix_<^
    val < = prefix_<^.<
    val ^ = prefix_<^.^
  }
  
  object all extends all
}
