package wicondev.ui.api

import autowire.ClientProxy
import upickle.{default => upickle}
import org.scalajs.dom

import scala.concurrent.Future
import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue

trait ApiClient[Api] extends autowire.Client[String, upickle.Reader, upickle.Writer] {
  val uri: Option[String] = None

  val proxy = ClientProxy[Api, String, upickle.Reader, upickle.Writer](this)

  override def doCall(req: Request): Future[String] = {
    dom.ext.Ajax.post(
      url     = s"/api${uri mkString("", "", "/")}${req.path.mkString("/")}",
      headers = Map(
        "Content-Type" -> "text/plain;charset=UTF-8"
      ),
      data    = upickle.write(req.args)
    ).map(_.responseText)
  }

  def read[Result: upickle.Reader](p: String) = upickle.read[Result](p)
  def write[Result: upickle.Writer](r: Result) = upickle.write(r)
}
